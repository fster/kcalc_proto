//devtool property for debugging, and we set this applications port to 8081.

module.exports = {
    
    devServer: {
        port: 8081
    },
    
    configureWebpack: {
     devtool: 'source-map'
    }

}