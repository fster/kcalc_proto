import http from "../http-common";

/*Import standard http functionality
 * then the method used for searching the database for entries matching the user's input.
 * The method takes the parameter and calls the http get method where the aforementioned parameter is fed into
 * and then returns the resulting data.*/

class DataService {

    findByPostal(input) {
        return http.get(`/stores?postalcode=${input}`);
    }
}


export default new DataService();