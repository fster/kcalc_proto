import axios from "axios";

/*Using axios for setting the server applications URL
*and setting the headers content type*/

export default axios.create({

    baseURL: "http://localhost:8080/api",
    headers: {

        "Content-type": "application-json"
    }


});