import Vue from "vue";
import Router from "vue-router";

/*Importing the standard vue functionality and routing functionality
 then tell Vue to use that router to be defined later in the code.
 In the 'routes' scope define the route to the components needing rendering after first load.
 No routes defined as only on component is used at the moment.*/
Vue.use(Router);

export default new Router({
    
    routes: [
        {

        }
    ]
});